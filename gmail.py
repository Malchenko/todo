# -*- coding: utf-8 -*-
from datetime import datetime, date, time, timedelta
import email
from imapclient import IMAPClient
import re


def run():
    HOST = 'imap.gmail.com'
    USERNAME = 'malchenko.dmitriy.by@gmail.com'
    PASSWORD = 'nMPYoz3A59bfFHFQVWlA'

    today = datetime.today()
    cutoff = today - timedelta(days=5)

    server = IMAPClient(HOST, use_uid=True, ssl=True)
    server.login(USERNAME, PASSWORD)
    select_info = server.select_folder('in-todo')
    messages = server.search(['ALL'])
    response = server.fetch([10], ['RFC822'])

    for msgid, data in response.iteritems():
        msg_string = data['RFC822']
        msg = email.message_from_string(msg_string)
        mail_from = get_mail_from(msg['From'])
        mail_date = get_mail_date(msg['Date'])
        mail_subject = get_mail_subject(msg['Subject'])
        mail_body = get_mail_body(msg)
        print mail_body
    server.close_folder()
    server.logout()


def get_mail_date(mail_date):
    return datetime.date(datetime.strptime(mail_date, "%a, %d %b %Y %H:%M:%S +0300"))


def get_mail_from(mail_from):
    result = re.findall(r'<(.*)>', mail_from)
    if not result:
        return ''
    return result[0]


def get_mail_body(msg):
    body = 'no body'
    if msg.is_multipart():
        for part in msg.walk():
            content_type = part.get_content_type()
            if content_type == 'text/plain':
                body = part.get_payload(decode=True)
    return body


def get_mail_subject(mail_subject):
    result = re.search(r'TODO', mail_subject)
    return result


def get_todo_header():
        week = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье']
        delta = datetime.timedelta(days=1)
        today = datetime.date.today()

        now_date = today
        next_date = today + delta
        workday = next_date.weekday()

        holidays = [datetime.date(2017, 05, 01), datetime.date(2017, 05, 8), datetime.date(2017, 05, 9),
                    datetime.date(2017, 12, 6), datetime.date(2017, 11, 6)];

        while workday > 4:
            next_date = next_date + delta
            workday = next_date.weekday()
        for i in holidays:
            if (i == next_date):
                workday = 5

        now = ('TODO ' + week[now_date.weekday()] + ' ' + now_date.strftime("%d.%m.%Y"));
        next = ('TODO ' + week[next_date.weekday()] + ' ' + next_date.strftime("%d.%m.%Y"));
        return [now, next]


def get_todo_template():
    f = open('template_todo')
    template = f.read()
    f.close()
    return template


def generate():
    todo_header = get_todo_header()
    template = get_todo_template()
    template = template.replace('#TODAY#', todo_header[0])
    template = template.replace('#TOMORROW#', todo_header[1])
    return template


# print generate()

run()

